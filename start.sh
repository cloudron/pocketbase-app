#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /app/data/{pb_data,pb_migrations,pb_hooks,pb_public}

[[ ! -f /app/data/pb_public/index.html ]] && cp /app/pkg/index.html /app/data/pb_public/index.html

echo "==> Update permissions"
chown -R cloudron:cloudron /app/data

init_db() {
    echo "==> Initialise database"
    gosu cloudron:cloudron /app/code/pocketbase serve --http 0.0.0.0:8090 --dir /app/data/pb_data --hooksDir /app/data/pb_hooks --publicDir /app/data/pb_public --migrationsDir /app/data/pb_migrations &
    pid=$!
    sleep 5

    while ! curl --fail -s http://localhost:8090 > /dev/null; do
        echo "=> Waiting for app to come up"
        sleep 1
    done

    echo "==> Stopping pocketbase"
    kill -SIGTERM ${pid} > /dev/null 2>&1
}

update_config() {
    echo "==> Update configuration"

    # We have to use email addon to configure email instead of sendmail. InsecureSkipVerify option discussion (https://github.com/pocketbase/pocketbase/discussions/669)
    senderName=$([[ -z ${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-} ]] && echo "PocketBase" ||  echo $CLOUDRON_MAIL_FROM_DISPLAY_NAME)
    app_settings=$(sqlite3 /app/data/pb_data/data.db "SELECT value FROM _params WHERE id='settings'" \
        | jq ".meta.appUrl=\"${CLOUDRON_APP_ORIGIN}\"" \
        | jq ".meta.senderName=\"${senderName}\"" \
        | jq ".meta.senderAddress=\"${CLOUDRON_MAIL_FROM}\"" \
        | jq ".smtp.enabled=true" \
        | jq ".smtp.host=\"${CLOUDRON_EMAIL_SERVER_HOST}\"" \
        | jq ".smtp.port=587" \
        | jq ".smtp.username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" \
        | jq ".smtp.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" \
        | jq ".smtp.authMethod=\"PLAIN\"" \
        | jq ".smtp.tls=false" \
        | jq ".smtp.localName=\"${CLOUDRON_APP_DOMAIN}\"" \
        | jq -c \
        | sed "s/'/''/g")

    sqlite3 /app/data/pb_data/data.db "UPDATE _params SET value='$app_settings' WHERE id='settings'"
}

if [[ ! -f /app/data/pb_data/data.db ]]; then
    echo "==> Init db on first run"
    init_db
    echo "==> Creating superuser"
    gosu cloudron:cloudron /app/code/pocketbase superuser --dir /app/data/pb_data --hooksDir /app/data/pb_hooks --publicDir /app/data/pb_public --migrationsDir /app/data/pb_migrations create admin@cloudron.local changeme123
fi

update_config

echo "==> Starting PocketBase"
exec gosu cloudron:cloudron /app/code/pocketbase serve --http 0.0.0.0:8090 --dir /app/data/pb_data --hooksDir /app/data/pb_hooks --publicDir /app/data/pb_public --migrationsDir /app/data/pb_migrations

