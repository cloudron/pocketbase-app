[0.1.0]
* Initial version

[0.2.0]
* Various manifest changes

[0.3.0]
* Update PocketBase to 0.22.13
* [Full changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.22.13)
* Fixed rules inconsistency for text literals when inside parenthesis (#5017).
* Updated Go deps.

[0.7.0]
* Update pocketbase to 0.22.29
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.22.13)
* (*Backported from v0.23.11*) Upgraded `golang.org/x/net` to 0.33.0 to fix [CVE-2024-45338](https://www.cve.org/CVERecord?id=CVE-2024-45338).
* (*Backported from v0.23.10*) Renew the superuser file token cache when clicking on the thumb preview or download link ([#&#8203;6137](https://github.com/pocketbase/pocketbase/discussions/6137)).
* (*Backported from v0.23.10*) Upgraded `modernc.org/sqlite` to 1.34.3 to fix "disk io" error on arm64 systems.
* Instead of unregistering the realtime client(s), we now just unset their auth state on delete of the related auth record so that the client(s) can receive the `delete` event ([#&#8203;5898](https://github.com/pocketbase/pocketbase/issues/5898)).
* (*Backported from v0.23.0-rc*) Added manual WAL checkpoints before creating the zip backup to minimize copying unnecessary data.
* Refresh the old collections state in the Import UI after successful import submission ([#&#8203;5861](https://github.com/pocketbase/pocketbase/issues/5861)).
* Added randomized throttle on failed filter list requests as a very rudimentary measure since some security researches raised concern regarding the possibity of eventual side-channel attacks.

[0.8.0]
* Update pocketbase to 0.24.1
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.22.13)
* Added missing time macros in the UI autocomplete.
* Fixed JSVM types for structs and functions with multiple generic parameters.
* Removed the "dry submit" when executing the collections Create API rule
* Changed the type definition of `store.Store[T any]` to `store.Store[K comparable, T any]` to allow support for custom store key types.
* Added `@yesterday` and `@tomorrow` datetime filter macros.
* Added `:lower` filter modifier (e.g. `title:lower = "lorem"`).
* Added `mailer.Message.InlineAttachments` field for attaching inline files to an email (*aka. `cid` links*).
* Added cache for the JSVM `arrayOf(m)`, `DynamicModel`, etc. dynamic `reflect` created types.
* Added auth collection select for the settings "Send test email" popup ([#&#8203;6166](https://github.com/pocketbase/pocketbase/issues/6166)).

[0.9.0]
* configure mail using email addon

[0.9.1]
* Update pocketbase to 0.24.2
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.22.13)
* Fixed display fields extraction when there are multiple "Presentable" `relation` fields in a single related collection ([#&#8203;6229](https://github.com/pocketbase/pocketbase/issues/6229)).

[0.9.2]
* Update pocketbase to 0.24.3
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.24.3)
* Fixed incorrectly reported unique validator error for fields starting with name of another field ([#&#8203;6281](https://github.com/pocketbase/pocketbase/pull/6281); thanks [@&#8203;svobol13](https://github.com/svobol13)).
* Reload the created/edited records data in the RecordsPicker UI.
* Updated Go dependencies.

[1.0.0]
* Update pocketbase to 0.24.4
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.24.4)
* Initial stable release
* Fixed fields extraction for view query with nested comments ([#&#8203;6309](https://github.com/pocketbase/pocketbase/discussions/6309)).
* Bumped GitHub action min Go version to 1.23.5 as it comes with some [minor security fixes](https://github.com/golang/go/issues?q=milestone%3AGo1.23.5).

[1.1.0]
* Update pocketbase to 0.25.0
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.0)
* Upgraded Google OAuth2 auth, token and userinfo endpoints to their latest versions.
* Added Trakt OAuth2 provider ([#&#8203;6338](https://github.com/pocketbase/pocketbase/pull/6338); thanks [@&#8203;aidan-](https://github.com/aidan-))
* Added support for case-insensitive password auth based on the related UNIQUE index field collation ([#&#8203;6337](https://github.com/pocketbase/pocketbase/discussions/6337)).
* Enforced `when_required` for the new AWS SDK request and response checksum validations to allow other non-AWS vendors to catch up with new AWS SDK changes (see [#&#8203;6313](https://github.com/pocketbase/pocketbase/discussions/6313) and [aws/aws-sdk-go-v2#2960](https://github.com/aws/aws-sdk-go-v2/discussions/2960)).
* Soft-deprecated `Record.GetUploadedFiles` in favor of `Record.GetUnsavedFiles` to minimize the ambiguities what the method do ([#&#8203;6269](https://github.com/pocketbase/pocketbase/discussions/6269)).
* Replaced archived `github.com/AlecAivazis/survey` dependency with a simpler  `osutils.YesNoPrompt(message, fallback)` helper.
* Upgraded to `golang-jwt/jwt/v5`.
* Added JSVM `new Timezone(name)` binding for constructing `time.Location` value ([#&#8203;6219](https://github.com/pocketbase/pocketbase/discussions/6219)).
* Added `inflector.Camelize(str)` and `inflector.Singularize(str)` helper methods.
* Use the non-transactional app instance during the realtime records delete access checks to ensure that cascade deleted records with API rules relying on the parent will be resolved.
* Other minor improvements (*replaced all `bool` exists db scans with `int` for broader drivers compatibility, updated API Preview sample error responses, updated UI dependencies, etc.*)

[1.1.1]
* Update pocketbase to 0.25.1
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.1)
* Fixed the batch API Preview success sample response.
* Bumped GitHub action min Go version to 1.23.6 as it comes with a [minor security fix](https://github.com/golang/go/issues?q=milestone%3AGo1.23.6+label%3ACherryPickApproved) for the ppc64le build.

[1.1.2]
* Update pocketbase to 0.25.3
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.3)
* Added a temporary exception for Backblaze S3 endpoints to exclude the new `aws-sdk-go-v2` checksum headers ([#&#8203;6440](https://github.com/pocketbase/pocketbase/discussions/6440)).
* Fixed realtime delete event not being fired for `RecordProxy`-ies and added basic realtime record resolve automated tests ([#&#8203;6433](https://github.com/pocketbase/pocketbase/issues/6433)).

[1.1.3]
* Update pocketbase to 0.25.4
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.4)
* Downgraded `aws-sdk-go-v2` to the version before the default data integrity checks because there have been reports for non-AWS S3 providers in addition to Backblaze (IDrive, R2) that no longer or partially work with the latest AWS SDK changes.

[1.1.4]
* Update pocketbase to 0.25.5
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.5)
* Set the current working directory as a default goja script path when executing inline JS strings to allow `require(m)` traversing parent `node_modules` directories.
* Updated `modernc.org/sqlite` and `modernc.org/libc` dependencies.

[1.1.5]
* Update pocketbase to 0.25.6
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.6)
* Restore the missing `meta.isNew` field of the OAuth2 success response ([#&#8203;6490](https://github.com/pocketbase/pocketbase/issues/6490)).
* Updated npm dependencies.

[1.1.6]
* Update pocketbase to 0.25.7
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.7)
* Fixed `@request.body.jsonObjOrArr.*` values extraction ([#&#8203;6493](https://github.com/pocketbase/pocketbase/discussions/6493)).

[1.1.7]
* Update pocketbase to 0.25.8
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.8)
* Added a default leeway of 5 minutes for the Apple/OIDC `id_token` timestamp claims check to account for clock-skew ([#&#8203;6529](https://github.com/pocketbase/pocketbase/issues/6529)).

[1.1.8]
* Update pocketbase to 0.25.9
* [Full Changelog](https://github.com/pocketbase/pocketbase/releases/tag/v0.25.9)
* Fixed `DynamicModel` object/array props reflect type caching ([#&#8203;6563](https://github.com/pocketbase/pocketbase/discussions/6563)).

[1.2.0]
* Update base image to 5.0.0

