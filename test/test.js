#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_EMAIL = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'changeme123';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//input[@type="email"]'));

        await browser.findElement(By.xpath('//input[@type="email"]')).sendKeys(ADMIN_EMAIL);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(ADMIN_PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "Login")]')).click();

        await waitForElement(By.xpath('//div[@class="breadcrumb-item" and contains(., "Collections")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await browser.sleep(2000);

        await waitForElement(By.xpath('//div[@aria-label="Logged superuser menu"]'));
        await browser.findElement(By.xpath('//div[@aria-label="Logged superuser menu"]')).click();

        await waitForElement(By.xpath('//button[contains(., "Logout")]'));
        await browser.findElement(By.xpath('//button[contains(., "Logout")]')).click();

        await waitForElement(By.xpath('//input[@type="email"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('can install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id io.pocketbase.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

});
