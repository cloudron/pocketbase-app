FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=pocketbase/pocketbase versioning=semver extractVersion=^v(?<version>.+)$
ARG POCKETBASE_VERSION=0.25.9

RUN curl -L https://github.com/pocketbase/pocketbase/releases/download/v${POCKETBASE_VERSION}/pocketbase_${POCKETBASE_VERSION}_linux_amd64.zip -o pocketbase.zip \
    && unzip pocketbase.zip -d /app/code \
    && rm pocketbase.zip

COPY index.html start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

