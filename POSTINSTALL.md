**Important:** Please keep in mind that PocketBase is still under active development and full backward compatibility is not guaranteed before reaching v1.0.0.

This app is pre-setup with an admin account. The initial credentials are:

**Email**: admin@cloudron.local<br/>
**Password**: changeme123<br/>

